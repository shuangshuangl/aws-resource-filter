package s3;


import java.time.Duration;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.File;
import org.apache.commons.io.FileUtils;

public class RivianOSNonVersioningFilterTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private JavascriptExecutor js;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "");
        driver = new ChromeDriver();
        baseUrl = "https://www.google.com/";
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30).toMillis(), TimeUnit.MILLISECONDS);
        js = (JavascriptExecutor) driver;
    }

    @Test
    public void testRivianOSS3DevTestCase() throws Exception {
        driver.get("https://d-9a673b624b.awsapps.com/start/#/");
        driver.get("https://d-9a673b624b.awsapps.com/start#/");
        driver.findElement(By.id("app-03e8643328913682")).click();
        driver.findElement(By.xpath("//portal-instance[@id='ins-9afafba7a74dc93d']/div/div/div/div[2]")).click();
        driver.findElement(By.linkText("Management console")).click();
        //ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
        driver.get("https://d-9a673b624b.awsapps.com/start/#/saml/custom/476306795673%20%28D_DC%29/Njk4NjY2NjIyNTUyX2lucy05YWZhZmJhN2E3NGRjOTNkX3AtM2JhMGRkMzU2YzFmYjI3Ng%3D%3D");
        driver.get("https://console.aws.amazon.com/console/home");
        driver.get("https://us-east-1.console.aws.amazon.com/console/home?region=us-east-1");
        driver.findElement(By.id("awsc-concierge-input")).click();
        driver.findElement(By.id("awsc-concierge-input")).clear();
        driver.findElement(By.id("awsc-concierge-input")).sendKeys("S3");
        driver.findElement(By.xpath("//div[@id='aws-unified-search-container']/div/div[2]/div/div/div[2]/div/div[2]/ul/li/div/div/ol/li/div/div/div[2]/div/h3/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/home?region=us-east-1");
        driver.get("https://s3.console.aws.amazon.com/s3/buckets?region=us-east-1");
        driver.findElement(By.xpath("//div[@id='buckets-table']/div/div/div/div[2]/div/div/div/div/div/div/input")).click();
        driver.findElement(By.xpath("//input[@value='dt-rivianos-apps']")).clear();
        driver.findElement(By.xpath("//input[@value='dt-rivianos-apps']")).sendKeys("dt-rivianos-apps");
        driver.findElement(By.linkText("dt-rivianos-apps")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?region=us-east-1&tab=objects");
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[3]/table/tbody/tr/td[2]/span/span/span/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?region=us-east-1&prefix=1.0.0/&showversions=false");
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[2]/div/div[2]/span/span/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/input")).click();
        driver.findElement(By.xpath("//input[@value='data-platform-portal']")).clear();
        driver.findElement(By.xpath("//input[@value='data-platform-portal']")).sendKeys("data-platform-portal");
        driver.findElement(By.xpath("//input[@value='data-platform-portal']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[3]/table/tbody/tr/td[2]/span/span/span/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?region=us-east-1&prefix=1.0.0/data-platform-portal/&showversions=false");
        driver.findElement(By.xpath("//div[@id='app']/div/div/div/div/div/main/div/div[2]/div/main/div/div/nav/ol/li[5]/div/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?prefix=1.0.0/&region=us-east-1");
        driver.findElement(By.xpath("//input[@value='data-platform-portal']")).click();
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[2]/div/div[2]/span/span/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/span[2]/button/span")).click();
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[2]/div/div[2]/span/span/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/input")).click();
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[2]/div/div[2]/span/span/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/input")).click();
        driver.findElement(By.xpath("//input[@value='dc-fe-app-admin-tools']")).clear();
        driver.findElement(By.xpath("//input[@value='dc-fe-app-admin-tools']")).sendKeys("dc-fe-app-admin-tools");
        driver.findElement(By.xpath("//input[@value='dc-fe-app-admin-tools']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[3]/table/tbody/tr/td[2]/span/span/span/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?region=us-east-1&prefix=1.0.0/dc-fe-app-admin-tools/&showversions=false");
        driver.findElement(By.xpath("//div[@id='app']/div/div/div/div/div/main/div/div[2]/div/main/div/div/nav/ol/li[5]/div/a/span")).click();
        driver.get("https://s3.console.aws.amazon.com/s3/buckets/dt-rivianos-apps?prefix=1.0.0/&region=us-east-1");
        driver.findElement(By.xpath("//awsui-table[@id='objects-table']/div/div[2]/div/div[2]/span/span/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/span[2]/button/span")).click();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
